# Packaging Pack

Packaging boilerplate for the buildpacks pack cli (https://buildpacks.io/docs/tools/pack/)

This is mainly used to create buildpack builders for the toolforge build service.

## Building the package

You can just use the build script, pass to it the version of pack that you want, and the version of the debian package
build, for example:
```
utils/build_deb.sh 0.27.0 2
```

That will build the package `build/pack-0.27.0-2.deb`.

## Uploading the package

Once built, you can upload the debian package with https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Packaging#Uploading_a_package
