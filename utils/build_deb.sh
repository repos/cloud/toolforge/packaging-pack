#!/bin/bash
set -o pipefail
set -o nounset
set -o errexit

PACK_VERSION=${1?No pack version passed}
DEB_VERSION=${2:-1}


docker build utils/debuilder-bullseye -t debuilder-bullseye:latest
docker run --volume $PWD:/src:rw --rm debuilder-bullseye:latest "$PACK_VERSION" "$DEB_VERSION"
