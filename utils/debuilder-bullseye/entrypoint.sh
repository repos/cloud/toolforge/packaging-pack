#!/bin/bash

set -o pipefail
set -o nounset
set -o errexit

PACK_VERSION=${1?No pack version passed}
DEB_VERSION=${2?No deb version passed}
FULL_VERSION=${PACK_VERSION}-${DEB_VERSION}


restore_user() {
    current_user=$(stat . --format=%u)
    current_group=$(stat . --format=%g)
    find . -user root -exec chown $current_user:$current_group {} \;
}

trap restore_user EXIT

cd /src
mkdir -p debian/pack/usr/local/bin

curl \
    --location \
    "https://github.com/buildpacks/pack/releases/download/v${PACK_VERSION}/pack-v${PACK_VERSION}-linux.tgz" \
    | tar --directory debian/pack/usr/local/bin -xvz

cd debian
sed -e "s/@@FULL_VERSION@@/${FULL_VERSION}/" pack/DEBIAN/control.tmpl > pack/DEBIAN/control
dpkg-deb --build pack "pack-${FULL_VERSION}.deb"

cd /src
rm -rf build
mkdir build
mv debian/*.deb build/
echo -e "\n\n###############################\nYour packages can be found now under:"
ls ./build/*
